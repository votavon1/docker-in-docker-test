FROM alpine:latest

RUN apk update && \
    apk add p7zip

COPY test.sh /app/test.sh

ENTRYPOINT /app/test.sh
